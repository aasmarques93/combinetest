//
//  AppCoordinator.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/21/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit

final class AppCoordinator: Coordinator {
    var window: UIWindow?
    
    private lazy var navigationController: UINavigationController = {
        UINavigationController(rootViewController: initialViewController)
    }()
    
    private lazy var initialViewController: UIViewController = {
        let viewController = UIStoryboard(storyboard: .list).instantiate(ListViewController.self)
        viewController.bind(to: ListViewModel(coordinator: self))
        return viewController
    }()
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
        
    func prepareTransition(for route: Route) {
        let viewController: UIViewController
        
        switch route {
        case .list:
            viewController = initialViewController
        case let .detail(path):
            let detailViewController = UIStoryboard(storyboard: .detail).instantiate(DetailViewController.self)
            detailViewController.bind(to: DetailViewModel(path: path))
            viewController = detailViewController
        }
        
        navigationController.pushViewController(viewController, animated: true)
    }
}
