//
//  Route.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/21/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit

enum Route {
    case list
    case detail(path: String?)
}
