//
//  Coordinator.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/21/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit

protocol Coordinator {
    func start()
    func prepareTransition(for route: Route)
}
