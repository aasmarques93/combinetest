//
//  Network.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/18/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import Foundation
import Combine

protocol NetworkProtocol {
    func request<T: Codable>(_ type: T.Type, url: NetworkURL, parameters: [String: Any]) -> AnyPublisher<T, Error>
    func requestData(path: String) -> AnyPublisher<Data, Error>
}

final class Network: NSObject {
    fileprivate let decoder = JSONDecoder()
    
    override init() {
        super.init()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }
}

extension Network: NetworkProtocol {
    func request<T: Codable>(_ type: T.Type,
                             url: NetworkURL,
                             parameters: [String: Any]) -> AnyPublisher<T, Error> {
        Just(url.createURL(with: parameters))
            .compactMap { $0?.url }
            .flatMap {
                URLSession.shared
                    .dataTaskPublisher(for: $0)
                    .catch { _ in Empty() }
            }
            .map { $0.data }
            .decode(type: T.self, decoder: decoder)
            .eraseToAnyPublisher()
    }
    
    func requestData(path: String) -> AnyPublisher<Data, Error> {
        Just(path)
            .compactMap { URL(string: $0) }
            .flatMap {
                URLSession.shared
                .dataTaskPublisher(for: $0)
                .catch { _ in Empty() }
            }
            .catch { _ in Empty() }
            .map { $0.data }
            .eraseToAnyPublisher()
    }
}
