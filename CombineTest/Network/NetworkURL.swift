//
//  NetworkURL.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/18/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import Foundation

enum NetworkURL: String {
    case photosSearch = "flickr.photos.search"
    case photoSizes = "flickr.photos.getSizes"
}

extension NetworkURL {
    private var scheme: String {
        return "https"
    }
    
    private var host: String {
        return "api.flickr.com"
    }
    
    private var path: String {
        return "/services/rest/"
    }
    
    private var apiKey: String {
        return "f9cc014fa76b098f9e82f1c288379ea1"
    }
}

extension NetworkURL {
    fileprivate var defaultQueryItems: [URLQueryItem] {
        var array = [URLQueryItem]()
        array.append(URLQueryItem(name: "method", value: rawValue))
        array.append(URLQueryItem(name: "format", value: "json"))
        array.append(URLQueryItem(name: "nojsoncallback", value: "1"))
        return array
    }
    
    fileprivate var apiKeyQueryItem: URLQueryItem {
        return URLQueryItem(name: "api_key", value: apiKey)
    }
}

extension NetworkURL {
    func createURL(with parameters: [String: Any]? = nil) -> URLRequest? {
        var components = URLComponents()
        
        components.scheme = scheme
        components.host = host
        components.path = path
        components.queryItems = createQueryItems(with: parameters)

        guard let url = components.url else {
            return nil
        }
        
        return URLRequest(url: url)
    }
    
    private func createQueryItems(with parameters: [String: Any]?) -> [URLQueryItem] {
        var queryItems = defaultQueryItems
        let parametersQueryItems = parameters?.map { URLQueryItem(name: $0.key, value: "\($0.value)") }
        queryItems.append(contentsOf: parametersQueryItems ?? [])
        queryItems.append(apiKeyQueryItem)
        return queryItems
    }
}
