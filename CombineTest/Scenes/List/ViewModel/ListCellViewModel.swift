//
//  ListCellViewModel.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/19/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import Foundation
import Combine

final class ListCellViewModel {
    private let network: NetworkProtocol
    private let photo: Photo
    
    private let dataSubject = PassthroughSubject<Data, Never>()
    let titleSubject = PassthroughSubject<String?, Never>()
    
    private(set) var largeURLPath = CurrentValueSubject<String?, Never>(nil)
    
    lazy var data: AnyPublisher<Data, Never> = {
        dataSubject.eraseToAnyPublisher()
    }()
    
    lazy var title: AnyPublisher<String?, Never> = {
        titleSubject.eraseToAnyPublisher()
    }()
    
    init(photo: Photo, network: NetworkProtocol) {
        self.photo = photo
        self.network = network
    }
    
    func fetchPhotoSizes() -> AnyCancellable? {
        let parameters: [String: Any] = [
            "photo_id": photo.id
        ]
        
        return network
            .request(APIResponse<Size>.self, url: .photoSizes, parameters: parameters)
            .handleEvents(receiveOutput: { (response) in
                let largeSize = response.results.filter { $0.label == "Large" }.first
                self.largeURLPath.send(largeSize?.source)
            })
            .map { self.generateRandomSize(from: $0.results).source }
            .flatMap { self.network.requestData(path: $0) }
            .receive(on: DispatchQueue.main)
            .catch { _ in Empty() }
            .sink { data in
                self.titleSubject.send(self.photo.title)
                self.dataSubject.send(data)
            }
    }
    
    private func generateRandomSize(from arraySizes: [Size]) -> Size {
        let randomIndex = Int.random(in: 0..<arraySizes.count)
        return arraySizes[randomIndex]
    }
}
