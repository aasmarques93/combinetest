//
//  ListViewModel.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/19/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import Foundation
import Combine

final class ListViewModel {
    private let network: NetworkProtocol
    private var coordinator: Coordinator?
    
    private let tags = "dogs"
    private let page = 1
    
    private let photosSubject = CurrentValueSubject<[ListCellViewModel], Never>([])
    
    lazy var photos: AnyPublisher<[ListCellViewModel], Never> = {
        photosSubject.eraseToAnyPublisher()
    }()
    
    init(network: NetworkProtocol = Network(), coordinator: Coordinator? = nil) {
        self.network = network
        self.coordinator = coordinator
    }
    
    func fetchContentData() -> AnyCancellable {
        requestPhotosSearch(with: tags, page: page)
            .map {
                $0.results.map { photo in
                    ListCellViewModel(photo: photo, network: self.network)
                }
            }
            .replaceError(with: [])
            .receive(on: DispatchQueue.main)
            .sink { self.photosSubject.send($0) }
    }
    
    private func requestPhotosSearch(with tags: String, page: Int = 1) -> AnyPublisher<APIResponse<Photo>, Error> {
        let parameters: [String: Any] = [
            "tags": tags,
            "page": page
        ]
        
        return network
            .request(APIResponse<Photo>.self, url: .photosSearch, parameters: parameters)
    }
    
    func didSelect(item: ListCellViewModel) {
        coordinator?.prepareTransition(for: .detail(path: item.largeURLPath.value))
    }
}
