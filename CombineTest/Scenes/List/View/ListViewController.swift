//
//  ListViewController.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/19/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit
import Combine

private let reuseIdentifier = "CellIdentifier"

final class ListViewController: UIViewController, Bindable {
    @IBOutlet private weak var tableView: UITableView!
    
    private var viewModel: ListViewModel?
    private var cancellables = [AnyCancellable]()
    private var dataSource: TableViewDataSource<ListCellViewModel, ListTableViewCell>?
    private var delegate: TableViewDelegate<ListCellViewModel>?
    
    func setupBindings(viewModel: ListViewModel) {
        self.viewModel = viewModel
        
        viewModel
            .fetchContentData()
            .store(in: &cancellables)
        
        viewModel
            .photos
            .sink { [weak self] (viewModels) in
                self?.setupDataSource(for: viewModels)
                self?.setupDelegate(for: viewModels)
                self?.tableView.reloadData()
            }
            .store(in: &cancellables)
    }
    
    private func setupDataSource(for objects: [ListCellViewModel]) {
        dataSource = TableViewDataSource(
            objects: objects,
            reuseIdentifier: reuseIdentifier
        ) { (object, cell) in
            cell.viewModel = object
            cell.setupBindings()
        }
        
        tableView.dataSource = dataSource
    }
    
    private func setupDelegate(for objects: [ListCellViewModel]) {
        delegate = TableViewDelegate(objects: objects, cellItem: { [weak self] (item) in
            self?.viewModel?.didSelect(item: item)
        })
        
        tableView.delegate = delegate
    }
}
