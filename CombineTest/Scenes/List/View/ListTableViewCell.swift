//
//  ListTableViewCell.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/19/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit
import Combine

final class ListTableViewCell: UITableViewCell {
    @IBOutlet private weak var imageViewPhoto: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    private var cancelables = [AnyCancellable]()
    var viewModel: ListCellViewModel?
    
    func setupBindings() {
        viewModel?
            .fetchPhotoSizes()?
            .store(in: &cancelables)
        
        viewModel?
            .title
            .assign(to: \.text, on: titleLabel)
            .store(in: &cancelables)
        
        viewModel?
            .data
            .compactMap { UIImage(data: $0) }
            .assign(to: \.image, on: imageViewPhoto)
            .store(in: &cancelables)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cancelables = []
    }
}
