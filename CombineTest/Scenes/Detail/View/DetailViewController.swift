//
//  DetailViewController.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/21/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit
import Combine

class DetailViewController: UIViewController, Bindable {
    @IBOutlet weak var imageView: UIImageView!
    
    var cancelables = [AnyCancellable]()
    
    func setupBindings(viewModel: DetailViewModel) {
        viewModel
            .fetchData()
            .store(in: &cancelables)
        
        viewModel
            .data
            .compactMap { UIImage(data: $0) }
            .assign(to: \.image, on: imageView)
            .store(in: &cancelables)
    }
}
