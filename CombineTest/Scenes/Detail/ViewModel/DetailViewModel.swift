//
//  DetailViewModel.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/21/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import Foundation
import Combine

final class DetailViewModel {
    private let network: NetworkProtocol
    
    private let dataSubject = PassthroughSubject<Data, Never>()
    lazy var data: AnyPublisher<Data, Never> = {
        dataSubject.eraseToAnyPublisher()
    }()
    
    private let path: String?
    
    init(path: String?, network: NetworkProtocol = Network()) {
        self.path = path
        self.network = network
    }
    
    func fetchData() -> AnyCancellable {
        network.requestData(path: path ?? "")
            .receive(on: DispatchQueue.main)
            .catch { _ in Empty() }
            .sink { data in
                self.dataSubject.send(data)
            }
    }
}
