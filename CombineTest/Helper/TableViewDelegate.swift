//
//  TableViewDelegate.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/21/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit

class TableViewDelegate<ObjectType>: NSObject, UITableViewDelegate {
    typealias CellItem = (ObjectType) -> Void

    private let objects: [ObjectType]
    private let cellItem: CellItem

    init(objects: [ObjectType],
         cellItem: @escaping CellItem) {
        
        self.objects = objects
        self.cellItem = cellItem
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < objects.count else {
            return
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        cellItem(objects[indexPath.row])
    }
}
