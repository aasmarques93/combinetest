//
//  Bindable.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/19/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit

protocol Bindable: class {
    associatedtype ViewModelType
    func setupBindings(viewModel: ViewModelType)
}

extension Bindable where Self: UIViewController {
    func bind(to viewModelType: ViewModelType) {
        loadViewIfNeeded()
        setupBindings(viewModel: viewModelType)
    }
}

extension Bindable where Self: UITableViewCell {
    func bind(to viewModelType: ViewModelType) {
        setupBindings(viewModel: viewModelType)
    }
}
