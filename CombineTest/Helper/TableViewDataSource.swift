//
//  TableViewDataSource.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/19/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit

final class TableViewDataSource<ObjectType, TableViewCellType: UITableViewCell>: NSObject, UITableViewDataSource {
    typealias CellConfigurator = (ObjectType, TableViewCellType) -> Void

    private let objects: [ObjectType]
    private let reuseIdentifier: String
    private let cellConfigurator: CellConfigurator

    init(objects: [ObjectType],
         reuseIdentifier: String,
         cellConfigurator: @escaping CellConfigurator) {
        
        self.objects = objects
        self.reuseIdentifier = reuseIdentifier
        self.cellConfigurator = cellConfigurator
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? TableViewCellType else {
            return UITableViewCell()
        }

        let object = objects[indexPath.row]
        cellConfigurator(object, cell)

        return cell
    }
}
