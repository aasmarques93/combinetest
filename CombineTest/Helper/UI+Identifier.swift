//
//  UI+Identifier.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/21/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit

protocol UIIdentifier { }

extension UIIdentifier where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}
extension UIViewController: UIIdentifier { }

extension UIIdentifier where Self: UICollectionViewCell {
    static var cellIdentifier: String {
        return String(describing: self)
    }
}
extension UICollectionViewCell: UIIdentifier { }
