//
//  GenericValue.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/18/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import Foundation

enum GenericValue: Codable {
    case int(Int)
    case string(String)
    case double(Double)
    case bool(Bool)
    case object([String: GenericValue])
    case array([GenericValue])
    
    func encode(to encoder: Encoder) throws {   
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        guard let value = ((try? container.decode(String.self)).map(GenericValue.string))
            .or((try? container.decode(Int.self)).map(GenericValue.int))
            .or((try? container.decode(Double.self)).map(GenericValue.double))
            .or((try? container.decode(Bool.self)).map(GenericValue.bool))
            .or((try? container.decode([String: GenericValue].self)).map(GenericValue.object))
            .or((try? container.decode([GenericValue].self)).map(GenericValue.array))
            else {
                throw DecodingError.typeMismatch(GenericValue.self, DecodingError.Context(codingPath: container.codingPath, debugDescription: "Not a JSON"))
        }
        
        self = value
    }
}

extension GenericValue {
    var intValue: Int? {
        switch self {
        case .double(let value):
            return Int(value)
        case .int(let value):
            return value
        case .string(let value):
            return Int(value)
        default: return nil
        }
    }
    
    var doubleValue: Double? {
        switch self {
        case .double(let value):
            return value
        case .int(let value):
            return Double(value)
        case .string(let value):
            return Double(value)
        default: return nil
        }
    }
    
    var stringValue: String? {
        switch self {
        case .double(let value):
            return String(value)
        case .int(let value):
            return String(value)
        case .string(let value):
            return value
        default: return nil
        }
    }
}
