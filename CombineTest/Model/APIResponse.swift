//
//  APIResponse.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/18/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import UIKit

enum APICodingKeyType: String, CodingKey {
    case photos, sizes
    
    fileprivate var customCodingKey: CustomCodingKey {
        switch self {
        case .photos:
            return .photo
        case .sizes:
            return .size
        }
    }
}

fileprivate enum CustomCodingKey: String, CodingKey {
    case photo, size
}

protocol APIEncodable: Codable {
    static var codingKeyType: APICodingKeyType { get }
}

struct APIResponse<T: APIEncodable>: Codable {
    let results: [T]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: APICodingKeyType.self)
        let object = try container.nestedContainer(keyedBy: CustomCodingKey.self, forKey: T.codingKeyType)
        results = try object.decode([T].self, forKey: T.codingKeyType.customCodingKey)
    }
}
