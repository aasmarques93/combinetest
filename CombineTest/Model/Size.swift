//
//  Size.swift
//  CombineTest
//
//  Created by Arthur Augusto Sousa Marques on 10/18/19.
//  Copyright © 2019 Arthur Augusto. All rights reserved.
//

import Foundation

struct Size: APIEncodable {
    static var codingKeyType: APICodingKeyType = .sizes
    
    let label: String
    let source: String
    let url: String
    let media: String
    let width: GenericValue
    let height: GenericValue
}
